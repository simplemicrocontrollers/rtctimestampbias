// RTC timestamp bias: Arduino with DS1307RTC exmaple
// https://gitlab.com/simplemicrocontrollers/rtctimestampbias
#include <Thread.h>
#include <DS1307RTC.h>

// Time bias correction variables:
long correctionCheck = 43200000;       // Offset in one second per the number of milliseconds
int correctionBias = 1;
Thread correctionThread = Thread();    // Time bias correction thread

void setup() {
  Serial.begin(9600);                  // Initializes the Serial connection @ 9600 baud for debug
  while (!Serial);                     // Wait until Arduino Serial Monitor opens
  setSyncProvider(RTC.get);            // The function to get the time from the RTC
  if(timeStatus()!= timeSet) {         // Checking the time setting
    Serial.println("Unable to sync with the RTC");
  } else {
//    Serial.println("RTC has set the system time");
    Serial.println(now());             // Sending a timestamp to the serial port if a RTC is detected
  }

  // Time bias correction thread initialization:
  correctionThread.onRun(correctionLoop);
  correctionThread.setInterval(correctionCheck);
}

void loop() {
  // Threads:
  if (correctionThread.shouldRun())
    correctionThread.run();
}

void correctionLoop() {
  tmElements_t RTCtime;
  RTC.read(RTCtime);
  time_t RTCtimestamp;
  RTCtimestamp = makeTime(RTCtime);
  tmElements_t timeNew;
  time_t newTimestamp = RTCtimestamp - correctionBias;
  breakTime(newTimestamp, timeNew);
  RTC.write(timeNew);
  setSyncProvider(RTC.get);
}