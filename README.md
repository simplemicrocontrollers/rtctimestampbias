# RTC timestamp bias

Saving and comparing timestamps of the microcontroller's real time clock

- `rtctimestampbias.py` - main script for saving and comparing timestamps (`serial` and `csv` packages are required)
- `rtctimestampbias.ino` - sample sketch for Arduino with DS1307RTC, in case of a daily single offset by a fixed number of seconds
- `rtctimestampbias_1sec.ino` - sample sketch for Arduino with DS1307RTC, in the case of a one-second offset by the number of milliseconds